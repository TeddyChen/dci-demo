package tw.teddysoft.dci.demo.step2.team.entity.role;

import tw.teddysoft.dci.demo.step0.team.entity.Board;
import tw.teddysoft.dci.demo.step0.team.entity.BoardRole;
import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BoardAccessRole implements TeamRoles.BoardAccess {
    private TeamRoles.BoardAccess.Data data;

    public BoardAccessRole(TeamRoles.BoardAccess.Data data) {
        super();
        this.data = data;
    }

    @Override
    public void createBoard(String boardId, String boardName) {
        if (getBoard(boardId).isPresent())
            throw new RuntimeException("Board is already in the team.");
        data.boards().add(new Board(data.teamId(), boardId, boardName));
    }

    @Override
    public void addBoardMember(String boardId, String userId, BoardRole role) {
        if (getBoard(boardId).get().getBoardMember(userId).isPresent()) {
            return;
        }
        this.getBoard(boardId).get().addMember(boardId, userId, role);
    }

    @Override
    public void removeBoardMember(String boardId, String userId) {
        if(!getBoard(boardId).get().getBoardMembers().stream().filter(x->x.userId().equals(userId)).findAny().isPresent()) {
            return;
        }
        this.getBoard(boardId).get().removeMember(userId);
    }

    @Override
    public Optional<Board> getBoard(String boardId) {
        return data.boards().stream().filter(x -> x.getId().equals(boardId)).findAny();
    }

    @Override
    public List<Board> getBoards() {
        return Collections.unmodifiableList(data.boards());
    }
}
