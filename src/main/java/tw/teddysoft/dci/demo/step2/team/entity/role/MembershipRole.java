package tw.teddysoft.dci.demo.step2.team.entity.role;

import tw.teddysoft.dci.demo.step0.team.entity.MembershipStatus;
import tw.teddysoft.dci.demo.step0.team.entity.TeamMember;
import tw.teddysoft.dci.demo.step0.team.entity.TeamRole;
import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MembershipRole implements TeamRoles.Membership {

    private TeamRoles.Membership.Data data;

    public MembershipRole(TeamRoles.Membership.Data data) {
        super();
        this.data = data;
    }

    @Override
    public void addMember(String userId, TeamRole teamRole, MembershipStatus membershipStatus) {
        TeamMember addedMember = new TeamMember(userId, data.teamId(), teamRole, membershipStatus);
        if (data.teamMembers().stream().filter(x -> x.equals(addedMember)).findAny().isEmpty()){
            data.teamMembers().add(addedMember);
        }
    }

    @Override
    public void removeTeamMember(String userId) {
        if(!data.teamMembers().stream().filter(x->x.userId().equals(userId)).findAny().isPresent()) {
            return;
        }
        data.teamMembers().removeIf(x->x.userId().equals(userId));
    }

    @Override
    public boolean isLastAdmin(String userId) {
        long numOfAdmins = getTeamMembers().stream().filter(x -> x.teamRole().equals(TeamRole.TeamAdmin)).count();
        return getTeamMember(userId).get().teamRole().equals(TeamRole.TeamAdmin) && numOfAdmins == 1;
    }

    @Override
    public Optional<TeamMember> getTeamMember(String userId) {
        return data.teamMembers().stream().filter(x->x.userId().equals(userId)).findAny();
    }

    @Override
    public List<TeamMember> getTeamMembers() {
        return Collections.unmodifiableList(data.teamMembers());
    }

}
