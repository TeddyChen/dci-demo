package tw.teddysoft.dci.demo.step3.team.entity;

import tw.teddysoft.dci.demo.common.AggregateRoot;
import tw.teddysoft.dci.demo.step0.team.entity.*;
import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;
import tw.teddysoft.dci.demo.step2.team.entity.role.BoardAccessRole;
import tw.teddysoft.dci.demo.step2.team.entity.role.MembershipRole;

import java.util.*;

public class Team extends AggregateRoot<String, TeamEvents> implements TeamRoles.Content, TeamRoles.Membership, TeamRoles.BoardAccess {

    private TeamRoles.TeamData data;
    private TeamRoles.Membership membership;
    private TeamRoles.BoardAccess boardAccess;

    public Team(String userId){
        super(UUID.randomUUID().toString());
        data = TeamRoles.TeamData.create();
        data.teamId(getId());

        membership = new MembershipRole(data);
        boardAccess = new BoardAccessRole(data);
    }

    @Override
    public void markAsDeleted(String userId) {
        isDeleted = true;
    }

    @Override
    public String getTeamId() {
        return getId();
    }

    @Override
    public void addMember(String userId, TeamRole teamRole, MembershipStatus membershipStatus) {
        membership.addMember(userId, teamRole, membershipStatus);
    }

    @Override
    public void removeTeamMember(String userId) {
        membership.removeTeamMember(userId);
    }

    @Override
    public boolean isLastAdmin(String userId) {
        return membership.isLastAdmin(userId);
    }

    @Override
    public Optional<TeamMember> getTeamMember(String userId) {
        return membership.getTeamMember(userId);
    }

    @Override
    public void createBoard(String boardId, String boardName) {
        boardAccess.createBoard(boardId, boardName);
    }

    @Override
    public List<TeamMember> getTeamMembers() {
        return Collections.unmodifiableList(membership.getTeamMembers());
    }

    @Override
    public void addBoardMember(String boardId, String userId, BoardRole role) {
        boardAccess.addBoardMember(boardId, userId, role);
    }

    @Override
    public void removeBoardMember(String boardId, String userId) {
        boardAccess.removeBoardMember(boardId, userId);
    }

    @Override
    public Optional<Board> getBoard(String boardId) {
        return boardAccess.getBoard(boardId);
    }

    @Override
    public List<Board> getBoards() {
        return Collections.unmodifiableList(boardAccess.getBoards());
    }

}
