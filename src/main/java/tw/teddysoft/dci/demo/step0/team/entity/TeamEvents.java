package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.DomainEvent;

import java.time.Instant;
import java.util.UUID;

public class TeamEvents implements DomainEvent {

    @Override
    public UUID id() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Instant occurredOn() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
