package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.ValueObject;

public enum TeamRole implements ValueObject {
    CompanyAdmin, TeamAdmin, Member
}
