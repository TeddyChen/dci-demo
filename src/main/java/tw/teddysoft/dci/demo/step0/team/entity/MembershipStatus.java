package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.ValueObject;

public enum MembershipStatus implements ValueObject {
    Active, Suspended, Pending
}
