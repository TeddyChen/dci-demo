package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.ValueObject;

public record BoardMember(String boardId, String userId, BoardRole role) implements ValueObject {
}
