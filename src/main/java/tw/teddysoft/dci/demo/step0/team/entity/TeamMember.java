package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.ValueObject;

public record TeamMember(String userId, String teamId, TeamRole teamRole, MembershipStatus membershipStatus) implements ValueObject {
}
