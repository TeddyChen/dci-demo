package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.ValueObject;

public enum BoardRole implements ValueObject {
    Admin, Member, Guest
}
