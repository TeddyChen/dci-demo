package tw.teddysoft.dci.demo.step0.team.entity;

import tw.teddysoft.dci.demo.common.Entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Board implements Entity {

    private String boardId;
    private String teamId;
    private String name;
    private List<BoardMember> boardMembers;

    protected Board(String boardId){this.boardId = boardId;}

    public Board(String teamId, String boardId, String name) {
       super();
       this.boardId = boardId;
       this.name = name;
       this.teamId = teamId;
       boardMembers = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamId() {
        return teamId;
    }

    public List<BoardMember> getBoardMembers() {
        return Collections.unmodifiableList(boardMembers);
    }

    public Optional<BoardMember> getBoardMember(String userId) {
        return boardMembers.stream().filter(x->x.userId().equals(userId)).findFirst();
    }

    public void addMember(String boardId, String userId, BoardRole role) {
        boardMembers.add(new BoardMember(boardId, userId, role));
    }
    public boolean removeMember(String userId) {
        return boardMembers.removeIf(x->x.userId().equals(userId));
    }


    @Override
    public boolean equals(Object that) {
        if(that instanceof Board) {
            Board thatBoard = (Board) that;
            return this.getId().equals(thatBoard.getId())  &&
                    this.getName().equals(thatBoard.getName()) &&
                    this.getTeamId().equals(thatBoard.getTeamId()) &&
                    this.getBoardMembers().equals(thatBoard.getBoardMembers());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 41 * (41 + getId().hashCode());
    }

    public String getId() {
        return boardId;
    }
}
