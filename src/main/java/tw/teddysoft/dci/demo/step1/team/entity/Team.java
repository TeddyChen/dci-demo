package tw.teddysoft.dci.demo.step1.team.entity;

import tw.teddysoft.dci.demo.common.AggregateRoot;
import tw.teddysoft.dci.demo.step0.team.entity.*;

import java.util.*;

public class Team extends AggregateRoot<String, TeamEvents> {
    private List<TeamMember> teamMembers;
    private List<Board> boards;

    public Team(String userId){
        super(UUID.randomUUID().toString());
        teamMembers = new ArrayList<>();
        boards = new ArrayList<>();
    }

    @Override
    public void markAsDeleted(String userId) {
        isDeleted = true;
    }

    public String getTeamId() {
        return getId();
    }

    public void addMember(String userId, TeamRole teamRole, MembershipStatus membershipStatus) {
        TeamMember addedMember = new TeamMember(userId, this.getTeamId(), teamRole, membershipStatus);

        if (teamMembers.stream().filter(x -> x.equals(addedMember)).findAny().isEmpty()){
            teamMembers.add(addedMember);
        }
    }

    public void removeTeamMember(String userId) {
        if(teamMembers.stream().filter(x->x.userId().equals(userId)).findAny().isPresent()) {
            teamMembers.removeIf(x->x.userId().equals(userId));
        }
    }

    public boolean isLastAdmin(String userId) {
        long numOfAdmins = getTeamMembers().stream().filter(x -> x.teamRole().equals(TeamRole.TeamAdmin)).count();
        return getTeamMember(userId).get().teamRole().equals(TeamRole.TeamAdmin) && numOfAdmins == 1;
    }

    public Optional<TeamMember> getTeamMember(String userId) {
        return teamMembers.stream().filter(x->x.userId().equals(userId)).findAny();
    }

    public void createBoard(String boardId, String boardName) {
        if (getBoard(boardId).isPresent())
            throw new RuntimeException("Board is already in the team.");

        boards.add(new Board(this.getTeamId(), boardId, boardName));
    }


    public List<TeamMember> getTeamMembers() {
        return Collections.unmodifiableList(teamMembers);
    }

    public void addBoard(Board board) {
        if (getBoard(board.getId()).isPresent())
            throw new RuntimeException("Board is already in the team.");

        boards.add(board);
    }

    public void addBoardMember(String boardId, String userId, BoardRole role) {
        if (getBoard(boardId).get().getBoardMember(userId).isPresent()) {
            return;
        }
        this.getBoard(boardId).get().addMember(boardId, userId, role);
    }

    public void removeBoardMember(String boardId, String userId) {
        if(!getBoard(boardId).get().getBoardMembers().stream().filter(x->x.userId().equals(userId)).findAny().isPresent()) {
            return;
        }
        this.getBoard(boardId).get().removeMember(userId);
    }

    public Optional<Board> getBoard(String boardId) {
        return boards.stream().filter(x -> x.getId().equals(boardId)).findAny();
    }

    public List<Board> getBoards() {
        return Collections.unmodifiableList(boards);
    }

}
