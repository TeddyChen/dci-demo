package tw.teddysoft.dci.demo.step1.team.entity;

import tw.teddysoft.dci.demo.step0.team.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface TeamRoles {

    interface Content{
        String getTeamId();

        interface Data {
            void teamId(String teamId);
            String teamId();
        }
    }

    interface Membership{
        void addMember(String userId, TeamRole teamRole, MembershipStatus membershipStatus);
        void removeTeamMember(String userId);
        boolean isLastAdmin(String userId);
        Optional<TeamMember> getTeamMember(String userId);
        List<TeamMember> getTeamMembers();

        interface Data {
            String teamId();
            List<TeamMember> teamMembers();
        }
    }

    interface BoardAccess {
        void createBoard(String boardId, String boardName);
        void addBoardMember(String boardId, String userId, BoardRole role);// diff member
        void removeBoardMember(String boardId, String userId);
        Optional<Board> getBoard(String boardId);
        List<Board> getBoards();
        interface Data {
            String teamId();
            List<Board> boards();
        }
    }

    interface TeamData extends Content.Data, Membership.Data, BoardAccess.Data {
        static TeamData create() {
            return new TeamDataImpl();
        }

        class TeamDataImpl implements TeamData {
            private String teamId;
            private List<TeamMember> teamMembers;
            private List<Board> boards;

            public TeamDataImpl(){
                teamMembers = new ArrayList<>();
                boards = new ArrayList<>();
            }

            @Override
            public void teamId(String teamId) {
                this.teamId = teamId;
            }

            @Override
            public String teamId() {
                return teamId;
            }

            @Override
            public List<Board> boards() {
                return boards;
            }

            @Override
            public List<TeamMember> teamMembers() {
                return teamMembers;
            }
        }
    }

}
