package tw.teddysoft.dci.demo.step5.team.entity.role;

import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;

public class BoardContentRole  {
    private TeamRoles.TeamData data;

    public BoardContentRole(TeamRoles.TeamData data) {
        super();
        this.data = data;
    }

    public void renameBoard(String boardId, String newName) {
        data.boards().stream().filter(x -> x.getId().equals(boardId)).findAny().ifPresent( x-> x.setName(newName));
    }

}
