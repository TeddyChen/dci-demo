package tw.teddysoft.dci.demo.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AggregateRoot<ID, E extends DomainEvent> implements Entity<ID> {

    protected boolean isDeleted;
    private final List<E> domainEvents;
    private ID id;


    public AggregateRoot(ID id) {
        this.id = id;
        domainEvents = new ArrayList<>();
    }

    @Override
    public ID getId(){
        return id;
    };

    public abstract void markAsDeleted(String userId);

    public boolean isDeleted(){ return  isDeleted;}

    public void clearDomainEvents() {
        domainEvents.clear();
    }

    public List<E> getDomainEvents() {
        return Collections.unmodifiableList(domainEvents);
    }

    protected void setId(ID id){
        this.id = id;
    }
}
