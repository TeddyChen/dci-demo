package tw.teddysoft.dci.demo.common;

import java.io.Serializable;

public interface Entity<ID> extends Serializable {
    ID getId();
}

