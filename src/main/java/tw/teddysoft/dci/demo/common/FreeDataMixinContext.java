package tw.teddysoft.dci.demo.common;

import java.util.*;

public class FreeDataMixinContext implements MixinContext {

    private final List<Object> behaviors;

    public static FreeDataMixinContext create(){
        return new FreeDataMixinContext();
    }

    public FreeDataMixinContext() {
        super();
        this.behaviors = new ArrayList();
    }

    public <T> T playRole(Class<T> roleClass) {
        Optional<T> behavior = behaviors.stream().parallel().
                filter(x -> roleClass.isAssignableFrom(x.getClass())).
                findAny().map(x -> (T) x);

        if (behavior.isPresent()) {
            return behavior.get();
        } else {
            throw new RuntimeException("Unsupported role: " + roleClass);
        }
    }

    @Override
    public void mixinBehavior(Object behavior, Object ...behaviors) {
        this.behaviors.add(behavior);
        for (Object each : behaviors) {
            this.behaviors.add(each);
        }
    }

    public List<Object> getBehaviors() {
        return Collections.unmodifiableList(behaviors);
    }

}
