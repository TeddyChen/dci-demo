package tw.teddysoft.dci.demo.common;

import java.util.List;

public interface MixinContext {

    void mixinBehavior(Object behavior, Object ...behaviors);

    <T> T playRole(Class<T> roleClass);

    List<Object> getBehaviors();
}
