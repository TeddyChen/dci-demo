package tw.teddysoft.dci.demo.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class DciAggregateRoot<ID, E extends DomainEvent>
        extends FreeDataMixinContext implements Entity<ID> {

    protected boolean isDeleted;
    private final List<E> domainEvents;
    private ID id;

    public DciAggregateRoot(ID id) {
        this.id = id;
        domainEvents = new ArrayList<>();
    }

    public void mixinBehavior(Class behaviorClass, Class dataClass) {
        try {
            Object behavior = behaviorClass.getConstructor(dataClass).newInstance(getData());
            mixinBehavior(behavior);
        } catch (Exception e) {
            throw new RuntimeException("Mixin behavior failed: " + e.getMessage(), e);
        }
    }

    @Override
    public ID getId(){
        return id;
    };

    public abstract void markAsDeleted(String userId);

    public boolean isDeleted(){ return  isDeleted;}

    public void clearDomainEvents() {
        domainEvents.clear();
    }

    public List<E> getDomainEvents() {
        return Collections.unmodifiableList(domainEvents);
    }

    protected void setId(ID id){
        this.id = id;
    }

    protected abstract Object getData();

}
