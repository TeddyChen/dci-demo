package tw.teddysoft.dci.demo.common;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

public interface DomainEvent extends Serializable {
	UUID id();
	Instant occurredOn();
}
