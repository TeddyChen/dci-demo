package tw.teddysoft.dci.demo.step3.team.entity.usecase;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tw.teddysoft.dci.demo.step0.team.entity.Board;
import tw.teddysoft.dci.demo.step0.team.entity.MembershipStatus;
import tw.teddysoft.dci.demo.step0.team.entity.TeamRole;
import tw.teddysoft.dci.demo.step3.team.entity.Team;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class LeaveTeamUseCaseTest {
    private static final String userId = "userId";

    @ParameterizedTest
    @ValueSource(strings = { "userId"})
    public void leave_team_use_case_succeeds_with_two_team_admins(String inputUserId) {
        Team team = CreateTeamUseCaseTest.createTeamUseCase(userId);
        team.addMember("userId2", TeamRole.TeamAdmin, MembershipStatus.Active);

        leaveTeamUseCase(team, inputUserId);

        assertEquals(1, team.getTeamMembers().size());
        assertTrue(team.getTeamMember("userId2").isPresent());
    }

    @ParameterizedTest
    @ValueSource(strings = { "userId"})
    public void leave_team_use_case_fails_with_one_team_admin(String inputUserId) {
        Team team = CreateTeamUseCaseTest.createTeamUseCase(userId);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leaveTeamUseCase(team, inputUserId);
        });
    }

    private void leaveTeamUseCase(Team team, String userId){
        for (Board each: team.getBoards()) {
            team.removeBoardMember(each.getId(), userId);
        }
        if(team.isLastAdmin(userId)) {
            throw new RuntimeException("Leave team member fail: Team requires at least one admin");
        }
        team.removeTeamMember(userId);
    }

}
