package tw.teddysoft.dci.demo.step5.team.usecase;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;
import tw.teddysoft.dci.demo.step4.team.entity.Team;
import tw.teddysoft.dci.demo.step4.team.entity.usecase.CreateTeamUseCaseTest;
import tw.teddysoft.dci.demo.step5.team.entity.role.BoardContentRole;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RenameBoardUseCaseTest {

    private static final String userId = "userId";
    private static final String boardId = "boardId";
    private static final String boardOldName = "boardOldName";
    private static final String boardNewName = "boardNewName";

    @Test
    public void rename_board_use_case() {
        Team team = CreateTeamUseCaseTest.createTeamUseCase(userId);
        team.playRole(TeamRoles.BoardAccess.class).createBoard(boardId, boardOldName);
        assertEquals(boardOldName, team.playRole(TeamRoles.BoardAccess.class).getBoard(boardId).get().getName());

        renameBoardUseCase(team, boardId, boardNewName);

        assertEquals(boardNewName, team.playRole(TeamRoles.BoardAccess.class).getBoard(boardId).get().getName());
    }

    private void renameBoardUseCase(Team team, String boardId, String boardNewName){
        team.mixinBehavior(BoardContentRole.class, TeamRoles.TeamData.class);
        team.playRole(BoardContentRole.class).renameBoard(boardId, boardNewName);
    }
}
