package tw.teddysoft.dci.demo.step2.team.entity.role;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import tw.teddysoft.dci.demo.step0.team.entity.MembershipStatus;
import tw.teddysoft.dci.demo.step0.team.entity.TeamMember;
import tw.teddysoft.dci.demo.step0.team.entity.TeamRole;
import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class MembershipRoleTest {

    private static final String userId = "userId";

    @Test
    public void add_a_team_member_with_the_team_admin_role() {
        TeamRoles.Membership.Data data = TeamRoles.TeamData.create();
        MembershipRole membershipRole = new MembershipRole(data);
        membershipRole.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        assertEquals(1, membershipRole.getTeamMembers().size());
        TeamMember teamMember = membershipRole.getTeamMember(userId).get();
        assertEquals(TeamRole.TeamAdmin, teamMember.teamRole());
        assertEquals(MembershipStatus.Active, teamMember.membershipStatus());
    }

    @Test
    public void remove_a_team_member() {
        TeamRoles.Membership.Data data = TeamRoles.TeamData.create();
        MembershipRole membershipRole = new MembershipRole(data);
        membershipRole.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        membershipRole.removeTeamMember(userId);

        assertEquals(0, membershipRole.getTeamMembers().size());
    }

    @Test
    public void remove_a_non_existing_team_member_has_no_effect() {
        TeamRoles.Membership.Data data = TeamRoles.TeamData.create();
        MembershipRole membershipRole = new MembershipRole(data);
        membershipRole.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        membershipRole.removeTeamMember("non-exist");

        assertEquals(1, membershipRole.getTeamMembers().size());
    }

    @Test
    public void is_last_team_admin_true() {
        TeamRoles.Membership.Data data = TeamRoles.TeamData.create();
        MembershipRole membershipRole = new MembershipRole(data);
        membershipRole.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        var result = membershipRole.isLastAdmin(userId);

        assertTrue(result);
    }

    @Test
    public void is_last_team_admin_with_two_admins_returns_false() {
        TeamRoles.Membership.Data data = TeamRoles.TeamData.create();
        MembershipRole membershipRole = new MembershipRole(data);
        membershipRole.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);
        membershipRole.addMember("userId2", TeamRole.TeamAdmin, MembershipStatus.Active);

        var result = membershipRole.isLastAdmin(userId);

        assertFalse(result);
    }

    @Test
    public void is_last_team_admin_with_one_admin_and_one_member_returns_true() {
        TeamRoles.Membership.Data data = TeamRoles.TeamData.create();
        MembershipRole membershipRole = new MembershipRole(data);
        membershipRole.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);
        membershipRole.addMember("userId2", TeamRole.Member, MembershipStatus.Active);

        var result = membershipRole.isLastAdmin(userId);

        assertTrue(result);
    }

}
