package tw.teddysoft.dci.demo.step2.team.entity;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import tw.teddysoft.dci.demo.step0.team.entity.*;

import static org.junit.jupiter.api.Assertions.*;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TeamTest {
    private static final String userId = "userId";
    private static final String boardId = "boardId";
    private static final String boardName = "boardName";

    @Test
    public void create_a_team() {
        Team team = new Team(userId);

        assertEquals(0, team.getTeamMembers().size());
    }

    @Test
    public void add_a_team_member_with_the_team_admin_role() {
        Team team = new Team(userId);

        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        assertEquals(1, team.getTeamMembers().size());
        TeamMember teamMember = team.getTeamMember(userId).get();
        assertEquals(TeamRole.TeamAdmin, teamMember.teamRole());
        assertEquals(MembershipStatus.Active, teamMember.membershipStatus());
    }

    @Test
    public void remove_a_team_member() {
        Team team = new Team(userId);
        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        team.removeTeamMember(userId);

        assertEquals(0, team.getTeamMembers().size());
    }

    @Test
    public void remove_a_non_existing_team_member_has_no_effect() {
        Team team = new Team(userId);
        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        team.removeTeamMember("non-exist");

        assertEquals(1, team.getTeamMembers().size());
    }

    @Test
    public void is_last_team_admin_true() {
        Team team = new Team(userId);
        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);

        var result = team.isLastAdmin(userId);

        assertTrue(result);
    }

    @Test
    public void is_last_team_admin_with_two_admins_returns_false() {
        Team team = new Team(userId);
        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);
        team.addMember("userId2", TeamRole.TeamAdmin, MembershipStatus.Active);

        var result = team.isLastAdmin(userId);

        assertFalse(result);
    }

    @Test
    public void is_last_team_admin_with_one_admin_and_one_member_returns_true() {
        Team team = new Team(userId);
        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);
        team.addMember("userId2", TeamRole.Member, MembershipStatus.Active);

        var result = team.isLastAdmin(userId);

        assertTrue(result);
    }

    @Test
    public void add_a_board() {
        Team team = new Team(userId);

        team.createBoard(boardId, boardName);

        Board addedBoard = team.getBoard(boardId).get();
        assertEquals(team.getTeamId(), addedBoard.getTeamId());
        assertEquals(boardId, addedBoard.getId());
        assertEquals(boardName, addedBoard.getName());
    }

    @Test
    public void add_a_board_member_with_the_board_admin_role() {
        Team team = new Team(userId);
        team.createBoard(boardId, boardName);

        team.addBoardMember(boardId, userId, BoardRole.Admin);

        Board addedBoard = team.getBoard(boardId).get();
        assertEquals(1, addedBoard.getBoardMembers().size());
        assertEquals(BoardRole.Admin, addedBoard.getBoardMembers().get(0).role());
    }

    @Test
    public void remove_an_existing_board_member() {
        Team team = new Team(userId);
        team.createBoard(boardId, boardName);
        team.addBoardMember(boardId, userId, BoardRole.Admin);

        team.removeBoardMember(boardId, userId);

        Board addedBoard = team.getBoard(boardId).get();
        assertEquals(0, addedBoard.getBoardMembers().size());
    }

    @Test
    public void remove_an_non_existing_board_member_has_no_effect() {
        Team team = new Team(userId);
        team.createBoard(boardId, boardName);
        team.addBoardMember(boardId, userId, BoardRole.Admin);

        team.removeBoardMember(boardId, "non-exist");

        Board addedBoard = team.getBoard(boardId).get();
        assertEquals(1, addedBoard.getBoardMembers().size());
    }
}
