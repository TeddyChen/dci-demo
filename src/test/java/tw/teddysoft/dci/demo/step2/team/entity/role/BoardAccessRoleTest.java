package tw.teddysoft.dci.demo.step2.team.entity.role;

import org.junit.jupiter.api.Test;
import tw.teddysoft.dci.demo.step0.team.entity.Board;
import tw.teddysoft.dci.demo.step0.team.entity.BoardRole;
import tw.teddysoft.dci.demo.step1.team.entity.TeamRoles;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardAccessRoleTest {

    private static final String boardName = "boardName";
    private static final String boardId = "boardId";
    private static final String userId = "userId";


    @Test
    public void add_a_board() {
        TeamRoles.BoardAccess.Data data = TeamRoles.TeamData.create();
        BoardAccessRole boardAccessRole = new BoardAccessRole(data);

        boardAccessRole.createBoard(boardId, boardName);

        Board addedBoard = boardAccessRole.getBoard(boardId).get();
        assertEquals(null, addedBoard.getTeamId());
        assertEquals(boardId, addedBoard.getId());
        assertEquals(boardName, addedBoard.getName());
        assertEquals(0, addedBoard.getBoardMembers().size());

    }

    @Test
    public void add_a_board_member_with_the_board_admin_role() {
        TeamRoles.BoardAccess.Data data = TeamRoles.TeamData.create();
        BoardAccessRole boardAccessRole = new BoardAccessRole(data);
        boardAccessRole.createBoard(boardId, boardName);

        boardAccessRole.addBoardMember(boardId, userId, BoardRole.Admin);

        Board addedBoard = boardAccessRole.getBoard(boardId).get();
        assertEquals(1, addedBoard.getBoardMembers().size());
        assertEquals(BoardRole.Admin, addedBoard.getBoardMembers().get(0).role());
    }

    @Test
    public void remove_an_existing_board_member() {
        TeamRoles.BoardAccess.Data data = TeamRoles.TeamData.create();
        BoardAccessRole boardAccessRole = new BoardAccessRole(data);
        boardAccessRole.createBoard(boardId, boardName);

        boardAccessRole.removeBoardMember(boardId, userId);

        Board board = boardAccessRole.getBoard(boardId).get();
        assertEquals(0, board.getBoardMembers().size());
    }

    @Test
    public void remove_an_non_existing_board_member_has_no_effect() {
        TeamRoles.BoardAccess.Data data = TeamRoles.TeamData.create();
        BoardAccessRole boardAccessRole = new BoardAccessRole(data);
        boardAccessRole.createBoard(boardId, boardName);

        boardAccessRole.removeBoardMember(boardId, "non-exist");

        Board board = boardAccessRole.getBoard(boardId).get();
        assertEquals(0, board.getBoardMembers().size());
    }

}
