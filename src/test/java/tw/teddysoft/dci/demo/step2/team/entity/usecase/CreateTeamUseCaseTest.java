package tw.teddysoft.dci.demo.step2.team.entity.usecase;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import tw.teddysoft.dci.demo.step0.team.entity.MembershipStatus;
import tw.teddysoft.dci.demo.step0.team.entity.TeamRole;
import tw.teddysoft.dci.demo.step2.team.entity.Team;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CreateTeamUseCaseTest {
    private static final String userId = "userId";

    public static Team createTeamUseCase(String userId){
        Team team = new Team(userId);
        team.addMember(userId, TeamRole.TeamAdmin, MembershipStatus.Active);
        return team;
    }

    @Test
    public void create_team_use_case() {

        Team team = createTeamUseCase(userId);

        assertEquals(1, team.getTeamMembers().size());
        assertEquals(TeamRole.TeamAdmin, team.getTeamMembers().get(0).teamRole());
    }

}
