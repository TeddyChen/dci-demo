package tw.teddysoft.dci.demo.step2.team.entity.usecase;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import tw.teddysoft.dci.demo.step0.team.entity.Board;
import tw.teddysoft.dci.demo.step0.team.entity.BoardRole;
import tw.teddysoft.dci.demo.step0.team.entity.MembershipStatus;
import tw.teddysoft.dci.demo.step0.team.entity.TeamRole;
import tw.teddysoft.dci.demo.step2.team.entity.Team;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InviteTeamMemberUseCaseTest {
    private static final String userId = "userId";

    @ParameterizedTest
    @EnumSource(TeamRole.class)
    public void invite_team_member_use_case(TeamRole teamRole) {
        Team team = CreateTeamUseCaseTest.createTeamUseCase(userId);
        team.createBoard("boardId1", "board1");
        team.createBoard("boardId2", "board2");

        inviteTeamMemberUseCase(team, userId, teamRole);

        Board board1 = team.getBoard("boardId1").get();
        assertEquals(1, board1.getBoardMembers().size());
        assertEquals(mapTeamRoleToBoardRole(teamRole), board1.getBoardMembers().get(0).role());
        Board board2 = team.getBoard("boardId1").get();
        assertEquals(1, board2.getBoardMembers().size());
        assertEquals(mapTeamRoleToBoardRole(teamRole), board2.getBoardMembers().get(0).role());
    }

    private void inviteTeamMemberUseCase(Team team, String userId, TeamRole teamRole){
        team.addMember(userId, teamRole, MembershipStatus.Active);
        for(Board each : team.getBoards()){
            team.addBoardMember(each.getId(), userId, mapTeamRoleToBoardRole(teamRole));
        }
    }

    private BoardRole mapTeamRoleToBoardRole(TeamRole teamRole) {
        return switch (teamRole) {
            case Member -> BoardRole.Member;
            case CompanyAdmin, TeamAdmin -> BoardRole.Admin;
            default -> BoardRole.Guest;
        };
    }

}
